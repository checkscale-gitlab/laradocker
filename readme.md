## Installation

Clone project

Use this command to include submodule: (we use [Laradock](https://laradock.io/) submodule)
```bash
git submodule add https://github.com/Laradock/laradock.git
```

Copy Laradock example ```.env``` (```/laradocker/.env```) to Laradock folder (```/laradocker/laradock/.env```):
```bash
cp .env.example ./laradock/.env
```

Change values of some keys in ```/laradock/.env``` file:
(row 27: ```COMPOSER_PROJECT_NAME=```; row 32: ```PHP_VERSION=```; row 47: ```PHP_IDE_CONFIG=serverName=```)
```bash
nano /laradock/.env
```

Build and Up docker using commands (from ```laradock``` folder):
```bash
cd laradock

docker-compose build

docker-compose up -d
```

Extract project files to ```project``` folder

Entry point in ```/project/public``` folder

Now you can use your docker using (from ```/laradocker/laradock``` folder):
```bash
docker-compose exec --user=laradock workspace bash
```

## IMPORTANT:

You can connect to MySQL (MariaDB) from PHPStorm using this params and then create new schema (DB) for project and manage it:
```
Host: localhost
Username: root
Password: root
```

Use this keys for connecting to DB from your Laravel Project (HOST Must be mariadb - this is your Docker Container):
```dotenv
DB_CONNECTION=mysql
DB_HOST=mariadb
DB_PORT=3306
DB_DATABASE=YOUR_DB_NAME
DB_USERNAME=root
DB_PASSWORD=root
```